import {
  BadRequestException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { CreateUserInput } from 'src/user/dto/create-user.input';
import { User } from 'src/user/entities/user.entity';
import { UserService } from 'src/user/user.service';
import { AuthHelper } from './auth.helper';
import { AuthLoginInput } from './dto/auth-login.input';
import { AuthRegisterInput } from './dto/auth-register.input';
import { JwtDto } from './dto/jwt.dto';
import { UserToken } from './entities/user-token.entity';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  async login(input: AuthLoginInput): Promise<UserToken> {
    const user = await this.userService.findUserByEmail(input.email);

    if (!user) {
      throw new NotFoundException(
        `User with email ${input.email} does not exist.`,
      );
    }

    const passwordValid = await AuthHelper.validate(
      input.password,
      user.password,
    );

    if (!passwordValid) {
      throw new UnauthorizedException('Invalid password');
    }

    return { user: user, token: this.signToken(user.id) };
  }

  async register(input: AuthRegisterInput): Promise<UserToken> {
    const user = await this.userService.findUserByEmail(input.email);

    if (user) {
      throw new BadRequestException(
        `Cannot register with email ${input.email}`,
      );
    }

    const hashedPassword = await AuthHelper.hash(input.password);
    const createdUser = await this.userService.save({
      ...input,
      password: hashedPassword,
    } as CreateUserInput);

    return { user: createdUser, token: this.signToken(createdUser.id) };
  }

  async save(input: AuthRegisterInput): Promise<UserToken> {
    const user = await this.userService.findUserByEmail(input.email);

    if (!user) {
      throw new BadRequestException(`Can't update User`);
    }

    const hashedPassword = await AuthHelper.hash(input.password);
    const updatedUser = await this.userService.save({
      ...input,
      password: hashedPassword,
    } as CreateUserInput);

    return { user: updatedUser, token: this.signToken(updatedUser.id) };
  }

  private signToken(id: number): string {
    const payload: JwtDto = { userId: id };
    return this.jwtService.sign(payload);
  }

  async validateUser(userId: number): Promise<User> {
    return this.userService.findOne(userId);
  }
}

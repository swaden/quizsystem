import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthLoginInput } from './dto/auth-login.input';
import { AuthRegisterInput } from './dto/auth-register.input';
import { UserToken } from './entities/user-token.entity';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/login')
  async login(@Body() authLoginInput: AuthLoginInput): Promise<UserToken> {
    return this.authService.login(authLoginInput);
  }

  @Post('/register')
  async register(
    @Body() authRegisterInput: AuthRegisterInput,
  ): Promise<UserToken> {
    return this.authService.register(authRegisterInput);
  }

  @Post('/settings')
  async update(
    @Body() authRegisterInput: AuthRegisterInput,
  ): Promise<UserToken> {
    return this.authService.save(authRegisterInput);
  }
}

import { User } from 'src/user/entities/user.entity';

export class UserToken {
  token: string;

  user: User;
}

import { IsNotEmpty, IsString } from 'class-validator';

export class AuthLoginInput {
  @IsNotEmpty()
  @IsString()
  email: string;

  @IsNotEmpty()
  @IsString()
  password: string;
}

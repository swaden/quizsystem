import { ExecutionContext, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class RestAuthGuard extends AuthGuard('jwt') {
  /**
   * Use @UseGuards(RestAuthGuard) decorator in controller or route handler to only allow logged in users
   */
  getRequest(context: ExecutionContext): any {
    const request = context.switchToHttp().getRequest();
    return request;
  }
}

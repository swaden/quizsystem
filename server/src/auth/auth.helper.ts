import { compare, hash } from 'bcryptjs';

export class AuthHelper {
  static validate(
    passwordInput: string,
    hashedPassword: string,
  ): Promise<boolean> {
    return compare(passwordInput, hashedPassword);
  }

  static hash(password: string): Promise<string> {
    return hash(password, 10);
  }
}

import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import { RestAuthGuard } from 'src/auth/guards/rest-auth.guard';
import { CreateSetInput } from './dto/create-set.input';
import { Set } from './entities/set.entity';
import { SetService } from './set.service';

@Controller('sets')
export class SetController {
  constructor(private setService: SetService) {}

  @UseGuards(RestAuthGuard)
  @Get()
  async findAll(): Promise<Set[]> {
    return this.setService.findAll();
  }

  @UseGuards(RestAuthGuard)
  @Get(':id')
  async findOne(@Param('id') id: number): Promise<Set> {
    return this.setService.findOne(id);
  }

  @UseGuards(RestAuthGuard)
  @Delete(':id')
  async deleteOne(@Param('id') id: number): Promise<Set> {
    return this.setService.deleteOne(id);
  }

  @UseGuards(RestAuthGuard)
  @Post()
  async save(@Body() createSetInput: CreateSetInput): Promise<Set> {
    return this.setService.save(createSetInput);
  }
}

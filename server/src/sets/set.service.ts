import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Game } from 'src/games/entities/game.entity';
import { Repository } from 'typeorm';
import { CreateSetInput } from './dto/create-set.input';
import { Set } from './entities/set.entity';

@Injectable()
export class SetService {
  constructor(
    @InjectRepository(Set)
    private setRepository: Repository<Set>,
  ) {}

  async findAll(): Promise<Set[]> {
    return this.setRepository.find({
      relations: ['questions'],
    });
  }

  async findOne(id: number): Promise<Set> {
    return this.setRepository.findOne({ where: { id: id } });
  }

  async deleteOne(id: number): Promise<any> {
    return this.setRepository.delete(id);
  }

  async findGames(set: Set): Promise<Game[]> {
    const data = await this.setRepository.findOne({
      where: { id: set.id },
      relations: ['games'],
    });
    return data.games;
  }

  async save(createSetInput: CreateSetInput): Promise<Set> {
    const newSet = this.setRepository.create(createSetInput);
    return this.setRepository.save(newSet);
  }

  async saveAll(createSetInputList: CreateSetInput[]): Promise<Set[]> {
    const newSetList = this.setRepository.create(createSetInputList);
    return this.setRepository.save(newSetList);
  }

  async findByIds(ids: number[]): Promise<Set[]> {
    return this.setRepository.findByIds(ids);
  }
}

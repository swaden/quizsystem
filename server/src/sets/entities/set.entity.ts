import { Game } from 'src/games/entities/game.entity';
import { Question } from 'src/questions/entities/question.entity';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'sets' })
export class Set {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'title', type: 'varchar', nullable: true })
  title?: string;

  @OneToMany(() => Game, (game) => game.set)
  games?: Game[];

  @ManyToMany(() => Question, (question) => question.sets, {
    cascade: true,
  })
  @JoinTable({ name: 'set_question' })
  questions?: Question[];
}

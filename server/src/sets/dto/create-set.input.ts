import { Game } from 'src/games/entities/game.entity';
import { Question } from 'src/questions/entities/question.entity';

export class CreateSetInput {
  id: number;
  title?: string;
  games?: Game[];
  questions?: Question[];
}

import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { Game } from './games/entities/game.entity';
import { GameModule } from './games/game.module';
import { Question } from './questions/entities/question.entity';
import { QuestionModule } from './questions/question.module';
import { Set } from './sets/entities/set.entity';
import { SetModule } from './sets/set.module';
import { User } from './user/entities/user.entity';
import { UserModule } from './user/user.module';
import { UserGame } from './usergame/entities/usergame.entity';
import { UserGameModule } from './usergame/game.module';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require('path');

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: path.join(process.cwd(), '../docker/.env'),
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'db',
      port: 3306,
      username: process.env.DB_ROOT_USER,
      password: process.env.DB_ROOT_PASSWORD,
      database: process.env.DB_NAME,
      entities: [Game, Question, User, Set, UserGame],
      synchronize: true,
    }),
    AuthModule,
    UserModule,
    QuestionModule,
    GameModule,
    SetModule,
    UserGameModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

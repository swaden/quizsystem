export class CreateUserInput {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  authToken?: string;
}

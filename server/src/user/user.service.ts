import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserInput } from './dto/create-user.input';
import { User } from './entities/user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
  ) {}

  async findAll(): Promise<User[]> {
    return this.userRepository.find();
  }

  async findOne(id: number): Promise<User> {
    return this.userRepository.findOne({ where: { id: id } });
  }

  async findUserByEmail(emailInput: string): Promise<User | undefined> {
    return this.userRepository.findOne({
      where: { email: emailInput },
    });
  }

  async save(createUserInput: CreateUserInput): Promise<User> {
    const newUser = this.userRepository.create(createUserInput);
    return this.userRepository.save(newUser);
  }

  async saveAll(createUserInputList: CreateUserInput[]): Promise<User[]> {
    const newUserList = this.userRepository.create(createUserInputList);
    return this.userRepository.save(newUserList);
  }
}

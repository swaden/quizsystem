import { Question } from 'src/questions/entities/question.entity';
import { UserGame } from 'src/usergame/entities/usergame.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'users' })
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'firstname', type: 'varchar' })
  firstName: string;

  @Column({ name: 'lastname', type: 'varchar' })
  lastName: string;

  @Column({ name: 'email', type: 'varchar' })
  email: string;

  @Column({ name: 'password', type: 'varchar' })
  password: string;

  @OneToMany(() => Question, (question) => question.user)
  questions?: Question[];

  @OneToMany(() => UserGame, (userGame) => userGame.user)
  userGames?: UserGame[];
}

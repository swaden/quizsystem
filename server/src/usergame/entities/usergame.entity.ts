import { Game } from 'src/games/entities/game.entity';
import { User } from 'src/user/entities/user.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'user_game' })
export class UserGame {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'right_answers', type: 'integer', nullable: true })
  rightAnswers?: number;

  @Column({ name: 'wrong_answers', type: 'integer', nullable: true })
  wrongAnswers?: number;

  @Column({
    name: 'timestamp',
    type: 'timestamp',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  timestamp?: string;

  @ManyToOne(() => User, (user) => user.userGames)
  user?: User;

  @ManyToOne(() => Game, (game) => game.userGames)
  game?: Game;
}

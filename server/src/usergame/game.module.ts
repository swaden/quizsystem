import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserGame } from './entities/usergame.entity';
import { UserGameController } from './usergame.controller';
import { UserGameService } from './usergame.service';

@Module({
  imports: [TypeOrmModule.forFeature([UserGame])],
  controllers: [UserGameController],
  providers: [UserGameService],
  exports: [UserGameService],
})
export class UserGameModule {}

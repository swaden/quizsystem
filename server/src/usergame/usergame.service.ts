import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Game } from 'src/games/entities/game.entity';
import { Repository } from 'typeorm';
import { CreateUserGameInput } from './dto/create-usergame.input';
import { UserGame } from './entities/usergame.entity';

@Injectable()
export class UserGameService {
  constructor(
    @InjectRepository(UserGame)
    private userGameRepository: Repository<UserGame>,
  ) {}

  async findAll(): Promise<Game[]> {
    return this.userGameRepository.find();
  }

  async findOne(id: number): Promise<UserGame> {
    return this.userGameRepository.findOne({ where: { id: id } });
  }

  async findPlayerCountByGameId(id: number): Promise<number> {
    const players = await this.userGameRepository.find({
      where: { game: { id: id } },
    });
    return players.length;
  }

  async findStatsByUserId(id: number): Promise<UserGame[]> {
    const allUserGames = await this.userGameRepository.find({
      where: { user: { id: id } },
      relations: ['user', 'game'],
    });
    return allUserGames;
  }

  async findByGameId(id: number): Promise<UserGame[]> {
    const allUserGames = await this.userGameRepository.find({
      where: { game: { id: id } },
      relations: ['user', 'game'],
    });
    return allUserGames;
  }

  async save(createUserGameInput: CreateUserGameInput): Promise<UserGame> {
    const newUserGame = this.userGameRepository.create(createUserGameInput);
    return this.userGameRepository.save(newUserGame);
  }

  async saveAll(
    createUserGameInputList: CreateUserGameInput[],
  ): Promise<UserGame[]> {
    const newUserGameList = this.userGameRepository.create(
      createUserGameInputList,
    );
    return this.userGameRepository.save(newUserGameList);
  }

  async findByIds(ids: number[]): Promise<UserGame[]> {
    return this.userGameRepository.findByIds(ids);
  }
}

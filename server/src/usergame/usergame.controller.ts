import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { RestAuthGuard } from 'src/auth/guards/rest-auth.guard';
import { CreateUserGameInput } from './dto/create-usergame.input';
import { UserGame } from './entities/usergame.entity';
import { UserGameService } from './usergame.service';

@Controller('user-games')
export class UserGameController {
  constructor(private userGameService: UserGameService) {}

  @UseGuards(RestAuthGuard)
  @Get()
  async findAll(): Promise<UserGame[]> {
    return this.userGameService.findAll();
  }

  @UseGuards(RestAuthGuard)
  @Get(':id')
  async findByGameId(@Param('id') id: number): Promise<UserGame[]> {
    return this.userGameService.findByGameId(id);
  }

  @UseGuards(RestAuthGuard)
  @Get('/stats/:id')
  async findStatsByUserId(@Param('id') id: number): Promise<UserGame[]> {
    return this.userGameService.findStatsByUserId(id);
  }

  @UseGuards(RestAuthGuard)
  @Get('/players/:id')
  async findPlayerCountByGameId(@Param('id') id: number): Promise<number> {
    return this.userGameService.findPlayerCountByGameId(id);
  }

  @UseGuards(RestAuthGuard)
  @Post()
  async register(
    @Body() createUserGameInput: CreateUserGameInput,
  ): Promise<UserGame> {
    return this.userGameService.save(createUserGameInput);
  }
}

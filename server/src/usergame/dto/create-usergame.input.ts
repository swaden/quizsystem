import { Game } from 'src/games/entities/game.entity';
import { User } from 'src/user/entities/user.entity';

export class CreateUserGameInput {
  id?: number;
  rightAnswers?: number;
  wrongAnswers?: number;
  timestamp?: string;
  user?: User;
  game?: Game;
}

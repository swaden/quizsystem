import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/user/entities/user.entity';
import { Repository } from 'typeorm';
import { CreateQuestionInput } from './dto/create-question.input';
import { Question } from './entities/question.entity';

@Injectable()
export class QuestionService {
  constructor(
    @InjectRepository(Question)
    private questionRepository: Repository<Question>,
  ) {}

  async findAll(): Promise<Question[]> {
    return this.questionRepository.find();
  }

  async findAllByUserId(id: number): Promise<Question[]> {
    return this.questionRepository.find({
      where: { user: { id: id } },
    });
  }

  async findOne(id: number): Promise<Question> {
    return this.questionRepository.findOne({ where: { id: id } });
  }

  async deleteOne(id: number): Promise<any> {
    return this.questionRepository.delete(id);
  }

  async findUser(question: Question): Promise<User> {
    const data = await this.questionRepository.findOne({
      where: { id: question.id },
      relations: ['user'],
    });
    return data.user;
  }

  async save(createQuestionInput: CreateQuestionInput): Promise<Question> {
    const newQuestion = this.questionRepository.create(createQuestionInput);
    return this.questionRepository.save(newQuestion);
  }

  async saveAll(
    createQuestionInputList: CreateQuestionInput[],
  ): Promise<Question[]> {
    const newQuestionList = this.questionRepository.create(
      createQuestionInputList,
    );
    return this.questionRepository.save(newQuestionList);
  }

  async findByIds(ids: number[]): Promise<Question[]> {
    return this.questionRepository.findByIds(ids);
  }
}

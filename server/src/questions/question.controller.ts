import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import { RestAuthGuard } from 'src/auth/guards/rest-auth.guard';
import { CreateQuestionInput } from './dto/create-question.input';
import { Question } from './entities/question.entity';
import { QuestionService } from './question.service';

@Controller('questions')
export class QuestionController {
  constructor(private questionService: QuestionService) {}

  @UseGuards(RestAuthGuard)
  @Get()
  async findAll(): Promise<Question[]> {
    return this.questionService.findAll();
  }

  @UseGuards(RestAuthGuard)
  @Get(':id')
  async findOne(@Param('id') id: number): Promise<Question> {
    return this.questionService.findOne(id);
  }

  @UseGuards(RestAuthGuard)
  @Get('/user/:id')
  async findAllByUserId(@Param('id') id: number): Promise<Question[]> {
    return this.questionService.findAllByUserId(id);
  }

  @UseGuards(RestAuthGuard)
  @Delete(':id')
  async deleteOne(@Param('id') id: number): Promise<Question> {
    return this.questionService.deleteOne(id);
  }

  @UseGuards(RestAuthGuard)
  @Post()
  async save(
    @Body() createQuestionInput: CreateQuestionInput,
  ): Promise<Question> {
    return this.questionService.save(createQuestionInput);
  }
}

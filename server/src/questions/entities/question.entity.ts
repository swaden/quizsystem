import { Set } from 'src/sets/entities/set.entity';
import { User } from 'src/user/entities/user.entity';
import {
  Column,
  Entity,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'questions' })
export class Question {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'title', type: 'varchar', nullable: true })
  title?: string;

  @Column({ name: 'tag', type: 'varchar', nullable: true })
  tag?: string;

  @Column({ name: 'answer1_right', type: 'varchar', nullable: true })
  answer1Right?: string;

  @Column({ name: 'answer2', type: 'varchar', nullable: true })
  answer2?: string;

  @Column({ name: 'answer3', type: 'varchar', nullable: true })
  answer3?: string;

  @Column({ name: 'answer4', type: 'varchar', nullable: true })
  answer4?: string;

  @Column({ name: 'explanation', type: 'text', nullable: true })
  explanation?: string;

  @ManyToOne(() => User, (user) => user.questions)
  user?: User;

  @ManyToMany(() => Set, (set) => set.questions)
  sets?: Set[];
}

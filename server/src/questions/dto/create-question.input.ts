import { User } from 'src/user/entities/user.entity';

export class CreateQuestionInput {
  id: number;
  title?: string;
  tag?: string;
  answer1Right?: string;
  answer2?: string;
  answer3?: string;
  answer4?: string;
  explanation?: string;
  user?: User;
}

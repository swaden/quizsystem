import { Set } from 'src/sets/entities/set.entity';
import { UserGame } from 'src/usergame/entities/usergame.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'games' })
export class Game {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'title', type: 'varchar', nullable: true })
  title?: string;

  @Column({ name: 'type', type: 'integer', nullable: true })
  type?: number;

  @ManyToOne(() => Set, (set) => set.games)
  set?: Set;

  @OneToMany(() => UserGame, (userGame) => userGame.game)
  userGames?: UserGame[];
}

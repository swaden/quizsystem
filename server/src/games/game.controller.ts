import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { RestAuthGuard } from 'src/auth/guards/rest-auth.guard';
import { CreateGameInput } from './dto/create-game.input';
import { Game } from './entities/game.entity';
import { GameService } from './game.service';

@Controller('games')
export class GameController {
  constructor(private gameService: GameService) {}

  @UseGuards(RestAuthGuard)
  @Get()
  async findAll(): Promise<Game[]> {
    return this.gameService.findAll();
  }

  @UseGuards(RestAuthGuard)
  @Get(':id')
  async findOne(@Param('id') id: number): Promise<Game> {
    return this.gameService.findOne(id);
  }

  @UseGuards(RestAuthGuard)
  @Post()
  async save(@Body() createGameInput: CreateGameInput): Promise<Game> {
    return this.gameService.save(createGameInput);
  }
}

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Set } from 'src/sets/entities/set.entity';
import { Repository } from 'typeorm';
import { CreateGameInput } from './dto/create-game.input';
import { Game } from './entities/game.entity';

@Injectable()
export class GameService {
  constructor(
    @InjectRepository(Game)
    private gameRepository: Repository<Game>,
  ) {}

  async findAll(): Promise<Game[]> {
    return this.gameRepository.find({
      relations: ['set', 'set.questions'],
    });
  }

  async findOne(id: number): Promise<Game> {
    return this.gameRepository.findOne({
      where: { id: id },
      relations: ['set', 'set.questions'],
    });
  }

  async findSet(game: Game): Promise<Set> {
    const data = await this.gameRepository.findOne({
      where: { id: game.id },
      relations: ['set'],
    });
    return data.set;
  }

  async save(createGameInput: CreateGameInput): Promise<Game> {
    const newGame = this.gameRepository.create(createGameInput);
    return this.gameRepository.save(newGame);
  }

  async saveAll(createGameInputList: CreateGameInput[]): Promise<Game[]> {
    const newGameList = this.gameRepository.create(createGameInputList);
    return this.gameRepository.save(newGameList);
  }

  async findByIds(ids: number[]): Promise<Game[]> {
    return this.gameRepository.findByIds(ids);
  }
}

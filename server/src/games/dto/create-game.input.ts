import { Set } from 'src/sets/entities/set.entity';

export class CreateGameInput {
  id: number;
  title?: string;
  type?: number;
  set?: Set;
}

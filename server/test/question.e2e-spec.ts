import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';

describe('QuestionController (e2e)', () => {
  let app: INestApplication;
  const token =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsImlhdCI6MTcxMTQ1NTk3Mn0.R5be43zYmtcLVPF6ZBOIkpB8yExP89FffQNjJpGoq2E';

  let createdId: number;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.setGlobalPrefix('api');
    app.enableCors();
    await app.init();
  });

  afterAll(async () => {
    await Promise.all([app.close()]);
  });

  describe('Authorization', () => {
    it('should return unauthorized', async () => {
      return await request(app.getHttpServer())
        .get('/api/questions')
        .expect(401);
    });

    it('post should return unauthorized', async () => {
      return await request(app.getHttpServer())
        .post('/api/questions/')
        .expect(401);
    });

    it('delete should return unauthorized', async () => {
      return await request(app.getHttpServer())
        .delete('/api/questions/20')
        .expect(401);
    });

    it('should return authorized', async () => {
      return await request(app.getHttpServer())
        .get('/api/questions')
        .set('Authorization', `Bearer ${token}`)
        .expect(200);
    });
  });

  describe('GetQuestions', () => {
    it('should return one game', async () => {
      return await request(app.getHttpServer())
        .get('/api/questions/9')
        .set('Authorization', `Bearer ${token}`)
        .expect(200)
        .expect(({ body }) => {
          expect(body.id).toEqual(9);
          expect(body.title).toEqual(expect.any(String));
        });
    });

    it('should return one game', async () => {
      return await request(app.getHttpServer())
        .get('/api/questions/')
        .set('Authorization', `Bearer ${token}`)
        .expect(200)
        .expect(({ body }) => {
          expect(Array.isArray(body)).toBe(true);
        });
    });
  });

  describe('PostQuestions', () => {
    it('should create a question', async () => {
      return await request(app.getHttpServer())
        .post('/api/questions/')
        .set('Authorization', `Bearer ${token}`)
        .expect(201)
        .expect(({ body }) => {
          expect(body.id).toEqual(expect.any(Number));
          createdId = body.id;
        });
    });
  });

  describe('DeleteQuestions', () => {
    it('should delete a question', async () => {
      return await request(app.getHttpServer())
        .delete(`/api/questions/${createdId}`)
        .set('Authorization', `Bearer ${token}`)
        .expect(200);
    });

    it('deleting question with wrong id should not work', async () => {
      return await request(app.getHttpServer())
        .delete(`/api/questions/test`)
        .set('Authorization', `Bearer ${token}`)
        .expect(500);
    });

    it('deleting question without id should not work', async () => {
      return await request(app.getHttpServer())
        .delete(`/api/questions/`)
        .set('Authorization', `Bearer ${token}`)
        .expect(404);
    });
  });
});

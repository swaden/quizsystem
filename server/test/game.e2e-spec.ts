import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';

describe('GameController (e2e)', () => {
  let app: INestApplication;
  const token =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsImlhdCI6MTcxMTQ1NTk3Mn0.R5be43zYmtcLVPF6ZBOIkpB8yExP89FffQNjJpGoq2E';

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.setGlobalPrefix('api');
    app.enableCors();
    await app.init();
  });

  afterAll(async () => {
    await Promise.all([app.close()]);
  });

  describe('Authorization', () => {
    it('should return unauthorized', async () => {
      return await request(app.getHttpServer()).get('/api/games').expect(401);
    });

    it('should return authorized', async () => {
      return await request(app.getHttpServer())
        .get('/api/games')
        .set('Authorization', `Bearer ${token}`)
        .expect(200);
    });
  });

  describe('GetGames', () => {
    it('should return one coop game', async () => {
      return await request(app.getHttpServer())
        .get('/api/games/1')
        .set('Authorization', `Bearer ${token}`)
        .expect(200)
        .expect(({ body }) => {
          expect(body.id).toEqual(1);
          expect(body.title).toEqual(expect.any(String));
          expect(body.type).toEqual(1);
          expect(body.set instanceof Object).toBe(true);
          expect(body.set.questions instanceof Array).toBe(true);
        });
    });

    it('should return one versus game', async () => {
      return await request(app.getHttpServer())
        .get('/api/games/2')
        .set('Authorization', `Bearer ${token}`)
        .expect(200)
        .expect(({ body }) => {
          expect(body.id).toEqual(2);
          expect(body.title).toEqual(expect.any(String));
          expect(body.type).toEqual(2);
          expect(body.set instanceof Object).toBe(true);
          expect(body.set.questions instanceof Array).toBe(true);
        });
    });

    it('should return one game', async () => {
      return await request(app.getHttpServer())
        .get('/api/games/')
        .set('Authorization', `Bearer ${token}`)
        .expect(200)
        .expect(({ body }) => {
          expect(Array.isArray(body)).toBe(true);
        });
    });

    it('should return nothing', async () => {
      return await request(app.getHttpServer())
        .get('/api/games/test')
        .set('Authorization', `Bearer ${token}`)
        .expect(200)
        .expect(({ body }) => {
          expect(body.id).toBeUndefined();
        });
    });
  });
});

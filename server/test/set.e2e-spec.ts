import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';

describe('SetController (e2e)', () => {
  let app: INestApplication;
  const token =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsImlhdCI6MTcxMTQ1NTk3Mn0.R5be43zYmtcLVPF6ZBOIkpB8yExP89FffQNjJpGoq2E';
  let createdId: number;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.setGlobalPrefix('api');
    app.enableCors();
    await app.init();
  });

  afterAll(async () => {
    await Promise.all([app.close()]);
  });

  describe('Authorization', () => {
    it('should return unauthorized', async () => {
      return await request(app.getHttpServer()).get('/api/sets').expect(401);
    });

    it('post should return unauthorized', async () => {
      return await request(app.getHttpServer()).post('/api/sets/').expect(401);
    });

    it('delete should return unauthorized', async () => {
      return await request(app.getHttpServer())
        .delete('/api/sets/20')
        .expect(401);
    });

    it('should return authorized', async () => {
      return await request(app.getHttpServer())
        .get('/api/sets')
        .set('Authorization', `Bearer ${token}`)
        .expect(200);
    });
  });

  describe('GetSets', () => {
    it('should return one set', async () => {
      return await request(app.getHttpServer())
        .get('/api/sets/1')
        .set('Authorization', `Bearer ${token}`)
        .expect(200)
        .expect(({ body }) => {
          expect(body.id).toEqual(1);
          expect(body.title).toEqual(expect.any(String));
        });
    });

    it('should return all sets', async () => {
      return await request(app.getHttpServer())
        .get('/api/sets/')
        .set('Authorization', `Bearer ${token}`)
        .expect(200)
        .expect(({ body }) => {
          expect(Array.isArray(body)).toBe(true);
        });
    });
  });

  describe('PostSets', () => {
    it('should create a set', async () => {
      return await request(app.getHttpServer())
        .post('/api/sets/')
        .set('Authorization', `Bearer ${token}`)
        .expect(201)
        .expect(({ body }) => {
          expect(body.id).toEqual(expect.any(Number));
          createdId = body.id;
        });
    });
  });

  describe('DeleteSets', () => {
    it('should delete a set', async () => {
      return await request(app.getHttpServer())
        .delete(`/api/sets/${createdId}`)
        .set('Authorization', `Bearer ${token}`)
        .expect(200);
    });

    it('deleting set with wrong id should not work', async () => {
      return await request(app.getHttpServer())
        .delete(`/api/sets/test`)
        .set('Authorization', `Bearer ${token}`)
        .expect(500);
    });

    it('deleting set without id should not work', async () => {
      return await request(app.getHttpServer())
        .delete(`/api/sets/`)
        .set('Authorization', `Bearer ${token}`)
        .expect(404);
    });
  });
});

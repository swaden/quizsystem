
# ISEF01-Quizsystem

Development of an interactive, user-friendly online quiz system for IU distance learning students that promotes cooperative and collaborative learning and provides a dynamic platform for exam preparation.

## Prerequisites

- Node.js and npm installed (or use inside the docker containers)
- Docker installed

## Installation

### Client

Navigate to the client folder:

```bash
cd client
```

Install dependencies:

```bash
npm install
```

To lint and check for errors:

```bash
npm run lint
```

To run builds:

```bash
npm run build
```

### Server

Navigate to the server folder:

```bash
cd server
```

Install dependencies:

```bash
npm install
```

To lint and check for errors:

```bash
npm run lint
```

To run unit tests (inside container):

```bash
npm run test
```

To run end-to-end tests (inside container):

```bash
npm run test:e2e
```

### Docker

Navigate to the docker folder:

```bash
cd docker
```

Copy the .env.sample file and fill it with your data:

```bash
cp .env.sample .env
```

For local usage, add the following entry to your /etc/hosts file:

```
127.0.0.1 quizsystem.loc
```

Start the containers:

```bash
docker-compose up -d
```

## Usage

### Analyzing Backend Logs

To monitor backend logs:

```bash
docker-compose logs -f backend
```
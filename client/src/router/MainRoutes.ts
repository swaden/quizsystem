const MainRoutes = {
  path: '/main',
  meta: {
    requiresAuth: true
  },
  redirect: '/main',
  component: () => import('@/layouts/dashboard/DashboardLayout.vue'),
  children: [
    {
      name: 'Dashboard',
      path: '/dashboard',
      component: () => import('@/views/dashboard/DefaultDashboard.vue')
    },
    {
      name: 'Games',
      path: '/games',
      component: () => import('@/views/games/MainPage.vue')
    },
    {
      name: 'Play',
      path: '/games/play/:gameId',
      component: () => import('@/views/games/PlayPage.vue')
    },
    {
      name: 'Set',
      path: '/manage/sets',
      component: () => import('@/views/manage/SetPage.vue')
    },
    {
      name: 'Questions',
      path: '/manage/questions',
      component: () => import('@/views/manage/QuestionPage.vue')
    },
    {
      name: 'Settings',
      path: '/settings',
      component: () => import('@/views/SettingPage.vue')
    }
  ]
};

export default MainRoutes;

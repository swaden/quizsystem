import { fetchWrapper } from '@/utils/helpers/fetch-wrapper';
import { defineStore } from 'pinia';

const baseUrl = import.meta.env.VITE_API_URL;

export const useSetStore = defineStore({
  id: 'set',
  state: () => ({
    sets: <any>[]
  }),
  actions: {
    async getAll() {
      const sets = await fetchWrapper.get(`${baseUrl}/sets`);

      this.sets = sets;
    },
    async save(set: any) {
      const newSet = await fetchWrapper.post(`${baseUrl}/sets`, {
        title: set?.title,
        questions: set?.questions
      });

      this.sets.push(newSet);
    },
    async update(set: any) {
      const updatedSet = await fetchWrapper.post(`${baseUrl}/sets`, {
        id: set?.id,
        title: set?.title,
        questions: set?.questions
      });

      const indexToUpdate = this.sets.findIndex((set: any) => set.id === updatedSet.id);
      if (indexToUpdate !== -1) {
        this.sets[indexToUpdate] = updatedSet;
      }
    },
    async delete(setId: number) {
      await fetchWrapper.delete(`${baseUrl}/sets/${setId}`);

      const indexToRemove = this.sets.findIndex((set: any) => set.id === setId);
      if (indexToRemove !== -1) {
        this.sets.splice(indexToRemove, 1);
      }
    }
  }
});

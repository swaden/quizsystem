import { fetchWrapper } from '@/utils/helpers/fetch-wrapper';
import { defineStore } from 'pinia';

const baseUrl = import.meta.env.VITE_API_URL;

export const useGameStore = defineStore({
  id: 'game',
  state: () => ({
    games: <any>[]
  }),
  actions: {
    async getAll() {
      const games = await fetchWrapper.get(`${baseUrl}/games`);

      this.games = games;
    },
    async getById(id: number) {
      const game = await fetchWrapper.get(`${baseUrl}/games/${id}`);
      return game;
    },
    async save(game: any) {
      const newGame = await fetchWrapper.post(`${baseUrl}/games`, {
        title: game?.title,
        type: game?.type,
        set: game?.set
      });

      this.games.push(newGame);
    },
    async savePlayedGame(game: any) {
      await fetchWrapper.post(`${baseUrl}/user-games`, {
        rightAnswers: game?.rightAnswers,
        wrongAnswers: game?.wrongAnswers,
        user: game?.user,
        game: game?.game
      });
    },
    async getPlayerCountByGameId(id: number) {
      return await fetchWrapper.get(`${baseUrl}/user-games/players/${id}`);
    },
    async getStatsByUserId(id: number) {
      return await fetchWrapper.get(`${baseUrl}/user-games/stats/${id}`);
    },
    async getByGameId(id: number) {
      return await fetchWrapper.get(`${baseUrl}/user-games/${id}`);
    }
  }
});

import { fetchWrapper } from '@/utils/helpers/fetch-wrapper';
import { defineStore } from 'pinia';

const baseUrl = import.meta.env.VITE_API_URL;

export const useQuestionStore = defineStore({
  id: 'question',
  state: () => ({
    questions: <any>[]
  }),
  actions: {
    async getAll() {
      const questions = await fetchWrapper.get(`${baseUrl}/questions`);

      this.questions = questions;
    },
    async getAllByUserId(id: number) {
      return await fetchWrapper.get(`${baseUrl}/questions/user/${id}`);
    },
    async save(question: any) {
      const newQuestion = await fetchWrapper.post(`${baseUrl}/questions`, {
        title: question?.title,
        tag: question?.tag,
        answer1Right: question?.answer1Right,
        answer2: question?.answer2,
        answer3: question?.answer3,
        answer4: question?.answer4,
        explanation: question?.explanation,
        user: question?.user
      });

      this.questions.push(newQuestion);
    },
    async update(question: any) {
      const updatedQuestion = await fetchWrapper.post(`${baseUrl}/questions`, {
        id: question?.id,
        title: question?.title,
        tag: question?.tag,
        answer1Right: question?.answer1Right,
        answer2: question?.answer2,
        answer3: question?.answer3,
        answer4: question?.answer4,
        explanation: question?.explanation
      });

      const indexToUpdate = this.questions.findIndex((question: any) => question.id === updatedQuestion.id);
      if (indexToUpdate !== -1) {
        this.questions[indexToUpdate] = updatedQuestion;
      }
    },
    async delete(questionId: number) {
      await fetchWrapper.delete(`${baseUrl}/questions/${questionId}`);

      const indexToRemove = this.questions.findIndex((question: any) => question.id === questionId);
      if (indexToRemove !== -1) {
        this.questions.splice(indexToRemove, 1);
      }
    }
  }
});

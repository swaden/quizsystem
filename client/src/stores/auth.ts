import { router } from '@/router';
import { fetchWrapper } from '@/utils/helpers/fetch-wrapper';
import { defineStore } from 'pinia';

const baseUrl = import.meta.env.VITE_API_URL;

export const useAuthStore = defineStore({
  id: 'auth',
  state: () => ({
    // initialize state from local storage to enable user to stay logged in
    /* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
    // @ts-ignore
    user: JSON.parse(localStorage.getItem('user')),
    /* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
    // @ts-ignore
    token: JSON.parse(localStorage.getItem('token')),
    returnUrl: null
  }),
  actions: {
    async login(email: string, password: string) {
      const user = await fetchWrapper.post(`${baseUrl}/auth/login`, { email, password });

      // update pinia state
      this.user = user.user;
      this.token = user.token;
      // store user details and jwt in local storage to keep user logged in between page refreshes
      localStorage.setItem('user', JSON.stringify(user.user));
      localStorage.setItem('token', JSON.stringify(user.token));
      // redirect to previous url or default to home page
      router.push(this.returnUrl || '/dashboard');
    },
    async register(email: string, firstName: string, lastName: string, password: string) {
      const user = await fetchWrapper.post(`${baseUrl}/auth/register`, { email, firstName, lastName, password });

      this.user = user.user;
      this.token = user.token;

      localStorage.setItem('user', JSON.stringify(user.user));
      localStorage.setItem('token', JSON.stringify(user.token));

      router.push(this.returnUrl || '/dashboard');
    },
    logout() {
      this.user = null;
      this.token = null;
      localStorage.removeItem('user');
      localStorage.removeItem('token');
      router.push('/auth/login');
    },
    async updateUser(id: number, email: string, firstName: string, lastName: string, password: string) {
      const user = await fetchWrapper.post(`${baseUrl}/auth/settings`, { id, email, firstName, lastName, password });

      this.user = user.user;
      this.token = user.token;

      localStorage.setItem('user', JSON.stringify(user.user));
      localStorage.setItem('token', JSON.stringify(user.token));
    }
  }
});

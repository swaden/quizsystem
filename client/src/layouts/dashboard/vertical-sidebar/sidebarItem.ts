// icons
import { QuestionOutlined, DashboardOutlined, InboxOutlined, RocketOutlined } from '@ant-design/icons-vue';

export interface menu {
  header?: string;
  title?: string;
  icon?: object;
  to?: string;
  divider?: boolean;
  children?: menu[];
  disabled?: boolean;
  type?: string;
  subCaption?: string;
}

const sidebarItem: menu[] = [
  { header: 'Navigation' },
  {
    title: 'Dashboard',
    icon: DashboardOutlined,
    to: '/dashboard'
  },
  { header: 'Spielen' },
  {
    title: 'Spiele',
    icon: RocketOutlined,
    to: '/games'
  },
  { header: 'Inhaltsverwaltung' },
  {
    title: 'Katalog',
    icon: InboxOutlined,
    to: '/manage/sets'
  },
  {
    title: 'Fragen',
    icon: QuestionOutlined,
    to: '/manage/questions'
  }
];

export default sidebarItem;
